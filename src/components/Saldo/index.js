import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import ButtonIcon from '../ButtonIcon';

export default class Saldo extends Component {
  render() {
    return (
      <View style={Styles.container}>
        <View style={{width: '55%'}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={Styles.saldo}>Saldo:</Text>
            <Text style={Styles.nominal}>Rp. 100.000</Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={Styles.antarPoint}>Antar Point:</Text>
            <Text style={Styles.point}>100 points</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'flex-end',
          }}>
          <ButtonIcon title="Add Saldo" />
          <ButtonIcon title="Get Point" />
        </View>
      </View>
    );
  }
}

// const winWidth = Dimensions.get('window').width;
const winHeight = Dimensions.get('window').height;
const Styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 17,
    marginHorizontal: 30,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginTop: -winHeight * 0.09,
    flexDirection: 'row',
  },

  saldo: {
    fontFamily: 'TitilliumWeb-Regular',
    fontSize: 20,
  },
  nominal: {fontFamily: 'TitilliumWeb-Bold', fontSize: 20},
  antarPoint: {fontFamily: 'TitilliumWeb-Regular', fontSize: 12},
  point: {fontFamily: 'TitilliumWeb-Bold', fontSize: 12, color: '#55CB95'},
});
