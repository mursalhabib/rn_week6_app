import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {
  IconAddSaldo,
  IconGetPoint,
  IconKiloan,
  IconSatuan,
  IconVip,
  IconKarpet,
  IconSetrika,
  IconEkspress,
} from '../../assets';

export default class ButtonIcon extends Component {
  Icon = () => {
    if (this.props.title === 'Add Saldo') return <IconAddSaldo />;
    if (this.props.title === 'Get Point') return <IconGetPoint />;
    if (this.props.title === 'Kiloan') return <IconKiloan />;
    if (this.props.title === 'Satuan') return <IconSatuan />;
    if (this.props.title === 'VIP') return <IconVip />;
    if (this.props.title === 'Karpet') return <IconKarpet />;
    if (this.props.title === 'Setrika Saja') return <IconSetrika />;
    if (this.props.title === 'Ekspress') return <IconEkspress />;
  };
  render() {
    return (
      <View>
        <TouchableOpacity
          style={Styles.container(this.props.type)}
          activeOpacity={0.5}>
          <View style={Styles.button(this.props.type)}>{this.Icon()}</View>
          <Text style={Styles.text(this.props.type)}>{this.props.title}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: type => ({
    marginBotton: 12,
    marginRight: type === 'layanan' ? 45 : 10,
  }),
  button: type => ({
    backgroundColor: '#E0F7EF',
    borderRadius: type === 'layanan' ? 10 : 5,
    padding: type === 'layanan' ? 12 : 7,
  }),
  text: type => ({
    marginBottom: type === 'layanan' ? 10 : 5,
    textAlign: 'center',
    fontSize: type === 'layanan' ? 14 : 10,
    fontFamily:
      type === 'layanan' ? 'TitilliumWeb-Light' : 'TitilliumWeb-Regular',
  }),
});
