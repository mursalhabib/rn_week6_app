import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {IconMesinCuci} from '../../assets';

export default class PesananAktif extends Component {
  constructor() {
    super();
    this.state = {
      pesanan: [
        {key: 1, nomor: '0001234', status: 'Sudah Selesai', color: '#55CB95'},
        {key: 2, nomor: '0003543', status: 'Masih Dicuci', color: '#FF4D00'},
        {key: 3, nomor: '0001363', status: 'Sudah Selesai', color: '#55CB95'},
        {key: 4, nomor: '0002156', status: 'Masih Dicuci', color: '#FF4D00'},
        {key: 5, nomor: '0001463', status: 'Sudah Selesai', color: '#55CB95'},
        {key: 6, nomor: '0001323', status: 'Sudah Selesai', color: '#55CB95'},
      ],
    };
  }
  render() {
    return (
      <View>
        {this.state.pesanan.map(item => {
          return (
            <View key={item.key} style={Styles.container}>
              <TouchableOpacity style={Styles.content} activeOpacity={0.8}>
                <IconMesinCuci />
                <View style={{marginLeft: 10}}>
                  <Text style={Styles.nomor}>Pesanan No. {item.nomor}</Text>
                  <Text
                    style={{
                      color: item.color,
                      fontFamily: 'TitilliumWeb-Light',
                      fontSize: 14,
                    }}>
                    {item.status}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {flexDirection: 'row', paddingVertical: 10},
  content: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 20,
    flexDirection: 'row',
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4,
    elevation: 4,
  },
  nomor: {
    fontFamily: 'TitilliumWeb-SemiBold',
    fontSize: 18,
  },
});
