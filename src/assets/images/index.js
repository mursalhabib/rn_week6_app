import Logo from './Logo.png';
import SplashBackground from './SplashBackground.png';
import Delivery from './Delivery.png';
import Ellipse from './Ellipse.png';

export {Logo, SplashBackground, Delivery, Ellipse};
