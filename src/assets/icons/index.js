import IconAkunActive from './akun.svg';
import IconAkun from './akungrey.svg';
import IconHomeActive from './home.svg';
import IconHome from './homegrey.svg';
import IconPesananActive from './pesanan.svg';
import IconPesanan from './pesanangrey.svg';
import IconAddSaldo from './isisaldo.svg';
import IconGetPoint from './isipoint.svg';
import IconKiloan from './kiloan.svg';
import IconSatuan from './satuan.svg';
import IconVip from './vip.svg';
import IconKarpet from './karpet.svg';
import IconSetrika from './setrika.svg';
import IconEkspress from './ekspress.svg';
import IconMesinCuci from './mesincuci.svg';

export {
  IconAkunActive,
  IconAkun,
  IconHomeActive,
  IconHome,
  IconPesananActive,
  IconPesanan,
  IconAddSaldo,
  IconGetPoint,
  IconKiloan,
  IconSatuan,
  IconVip,
  IconKarpet,
  IconSetrika,
  IconEkspress,
  IconMesinCuci,
};
