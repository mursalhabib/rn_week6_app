import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class Pesanan extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{fontFamily: 'TitilliumWeb-SemiBold', fontSize: 18}}>
          Halaman Pesanan
        </Text>
        <Text style={{fontFamily: 'TitilliumWeb-Bold', fontSize: 24}}>
          Coming Soon
        </Text>
      </View>
    );
  }
}
