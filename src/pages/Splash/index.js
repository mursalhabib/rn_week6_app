import React, {Component} from 'react';
import {Text, View, ImageBackground, Image} from 'react-native';
import {Logo, SplashBackground} from '../../assets';

export default class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('mainApp');
    }, 2500);
  }

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          backgroundColor: '#E0F7EF',
        }}>
        <ImageBackground
          source={SplashBackground}
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image source={Logo} style={{width: 222, height: 105}} />
        </ImageBackground>
      </ImageBackground>
    );
  }
}
