import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {Logo, Delivery, Ellipse} from '../../assets';

import {Saldo, ButtonIcon, PesananAktif} from '../../components';

export default class PagesHome extends Component {
  render() {
    return (
      <ScrollView style={{backgroundColor: 'white'}}>
        <>
          <ImageBackground style={Styles.bgImage} source={Ellipse}>
            <Image source={Delivery} style={Styles.deliveryImage} />
            <Image source={Logo} style={Styles.logo} />
            <View style={Styles.content}>
              <Text style={Styles.welcome}>Selamat Datang,</Text>
              <Text style={Styles.nama}>Habib</Text>
            </View>
          </ImageBackground>

          {/* Section Saldo */}
          <Saldo />

          {/* Section Layanan */}
          <View style={Styles.layananContainer}>
            <Text style={Styles.judulLayanan}>Layanan Kami</Text>
            <View style={Styles.baris}></View>
            <View style={Styles.layanan}>
              <ButtonIcon title="Kiloan" type="layanan" />
              <ButtonIcon title="Satuan" type="layanan" />
              <ButtonIcon title="VIP" type="layanan" />
              <ButtonIcon title="Karpet" type="layanan" />
              <ButtonIcon title="Setrika Saja" type="layanan" />
              <ButtonIcon title="Ekspress" type="layanan" />
            </View>
          </View>

          {/* Section Pesanan */}
          <View style={Styles.pesananContainer}>
            <Text style={Styles.textPesananAktif}>Pesanan Aktif</Text>
            <View style={Styles.baris2}></View>

            <PesananAktif />
          </View>
        </>
      </ScrollView>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Styles = StyleSheet.create({
  layananContainer: {paddingLeft: 45, paddingTop: 15},
  judulLayanan: {fontFamily: 'TitilliumWeb-Bold', fontSize: 18},
  layanan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    flexWrap: 'wrap',
  },
  baris: {
    backgroundColor: '#55CB95',
    padding: 1,
    width: 67,
  },
  baris2: {
    backgroundColor: '#55CB95',
    padding: 1,
    width: 67,
    marginLeft: 15,
  },
  pesananContainer: {
    marginTop: 10,
    paddingHorizontal: 30,
    paddingTop: 10,
    backgroundColor: '#f6f6f6',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderWidth: 2,
    borderColor: '#55CB95',
    flex: 1,
  },
  textPesananAktif: {
    fontFamily: 'TitilliumWeb-Bold',
    fontSize: 18,
    paddingLeft: 15,
  },
  bgImage: {
    height: 230,
    width: windowWidth,
    paddingHorizontal: 30,
    paddingTop: 10,
    borderBottomEndRadius: 50,
    borderBottomStartRadius: 50,
  },
  deliveryImage: {
    position: 'absolute',
    top: windowHeight * 0.0369,
    right: windowWidth * 0.04,
    width: 170,
    height: 190,
  },
  logo: {
    width: windowWidth * 0.3,
    height: windowHeight * 0.08,
  },
  content: {marginTop: 20},
  welcome: {fontFamily: 'TitilliumWeb-Regular', fontSize: 24},
  nama: {fontFamily: 'TitilliumWeb-Bold', fontSize: 24},
});
