import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Home, Splash, Akun, Pesanan} from '../pages';
import {
  IconAkunActive,
  IconAkun,
  IconHomeActive,
  IconHome,
  IconPesananActive,
  IconPesanan,
} from '../assets/icons';

export default class Index extends Component {
  mainApp = () => {
    const Tab = createBottomTabNavigator();
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            if (route.name === 'Home')
              return focused ? <IconHomeActive /> : <IconHome />;
            if (route.name === 'Pesanan')
              return focused ? <IconPesananActive /> : <IconPesanan />;
            if (route.name === 'Akun')
              return focused ? <IconAkunActive /> : <IconAkun />;
          },
          tabBarButton: props => (
            <TouchableOpacity activeOpacity={0.5} {...props} />
          ),
        })}
        tabBarOptions={{
          activeTintColor: '#55CB95',
          inactiveTintColor: '#C8C8C8',
          style: {height: 77, paddingTop: 10, paddingBottom: 10},
        }}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Pesanan" component={Pesanan} />
        <Tab.Screen name="Akun" component={Akun} />
      </Tab.Navigator>
    );
  };

  render() {
    const Stack = createStackNavigator();
    return (
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="mainApp"
          component={this.mainApp}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  }
}
